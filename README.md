# widgets

<!-- MarkdownTOC autolink="true" autoanchor="true" -->

- [Get started](#get-started)
- [Contribute](#contribute)
- [Troubleshoot](#troubleshoot)
- [Credits](#credits)

<!-- /MarkdownTOC -->


<a id="get-started"></a>
## Get started ##


<a id="contribute"></a>
## Contribute ##


<a id="troubleshoot"></a>
## Troubleshoot ##

<a id="credits"></a>
## Credits ##

* author: energenious GbR
* year: 2020
* contact: [opensource@energenious.eu](mailto:opensource@energenious.eu)






